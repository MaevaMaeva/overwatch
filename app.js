//fonction qui récupère les données grace au battletag et qui vérifie au passage
const recup = () =>{
    verifNom();
    verifPrenom();
    //verifBattletag(); qui ne marche pas
    //verifDate(); qui ne marche pas
    let battletag = document.getElementById('battletag');
    //remplacer le # par un tiret :
    const remplacement = (battletag.value).replace('#', '-');
    console.log(testBattletag(battletag));
    // requete fetch :
    fetch('https://ow-api.com/v1/stats/pc/us/' + remplacement + '/complete')
    .then(reponse=> reponse.json())
    .then(data =>{

        //peut s'écrire de 2 façons, genre :
        let partiesPerdues = data['competitiveStats']['games']['played']+data['quickPlayStats']['games']['played']-data['competitiveStats']['games']['won']-data['quickPlayStats']['games']['won'];
        //ou :
        let partiesGagnees = data.gamesWon;
        let classement = data.rating;
        let partiesJouees = partiesGagnees + partiesPerdues;
        
        let pj = document.getElementById('ajoutPJ');
        // =>pj = document.querySelector('#ajoutPJ').value, # pour id, . pour classe, value car query selector réccupère l'objet
        pj.innerHTML= partiesJouees;
        // => pj.textContent = partiesJouees, ou encore pj.innerText = PartiesJouees, la différence entre les 3 reste obscure !
        let pp = document.getElementById('ajoutPP');
        pp.innerHTML = partiesPerdues;
        let c = document.getElementById('ajoutC');
        c.innerHTML = classement;
        let pg = document.getElementById('ajoutPG');
        pg.innerHTML = partiesGagnees;
    })
}

//fonction qui teste si présence d'un décimal dans une chaine de caractères :
const testDigit = valeurString => {
    const DigitRegExp = /\d/;
    return DigitRegExp.test(valeurString);
}

//fonction qui vérifie le nom
const verifNom = () =>{
    const nom = document.getElementById('lastname');
    

    if (nom.value.length <3){
        alert('Merci d\'entrer un nom de plus de 2 caractères');
    }else if (nom.value.length >15){
        alert('Merci d\'entrer un nom moins long');
    }else if (testDigit(nom.value)){
        alert('Merci de ne pas entrer de chiffre dans le nom');
    }else {
        console.log('Le nom est bien renseigné');
    }
 }

 //fonction qui vérifie le prénom
 const verifPrenom = ()=>{
    const prenom = document.getElementById('firstname');
    if (prenom.value.length <3){
        alert('Merci d\'entrer un prénom de plus de 2 caractères');
    }else if (prenom.value.length >15){
        alert('Merci d\'entrer un prénom moins long');
    }else if (testDigit(prenom.value)){
        alert('Merci de ne pas entrer de chiffre dans le prénom');
    }else {
        console.log('Le prénom est bien renseigné');
    }
 }
 
//partie qui ne fonctionne pas
const verifDate = () =>{
    let jour = getDate();
    if (day>jour){
        alert('Vous n\'etes pas né !');
    }else{
        console.log('La date de naissance est renseignée');
    }
}
const testBattletag = (valeurPseudo) => {
        const BTRegExp = /(^([A-zÀ-ú][A-zÀ-ú0-9]{2,11})|(^([а-яёА-ЯЁÀ-ú][а-яёА-ЯЁ0-9À-ú]{2,11})))(#[0-9]{4,})$/;
        return BTRegExp.test(valeurPseudo);
}
const verifBattletag = () =>{
        let battletag = document.getElementById('battletag');
        if (testBattletag(battletag)){
            console.log('Le battletag est bien renseigné');
        }else{
            alert('Veuillez entrer un battletag sous la forme pseudo#chiffres');
        }
}